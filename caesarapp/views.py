from django.http import HttpResponse,JsonResponse
from django.views.generic import TemplateView
from rest_framework.decorators import api_view
from caesar import caesar
from rest_framework.parsers import JSONParser


class Index(TemplateView):
	template_name = 'index.html'


@api_view(['POST'])
def cesar_code(request):
	if request.method == 'POST':
		data = JSONParser().parse(request)
		message = caesar(data.get('enter_msg'), data.get('crypt_key'), data.get('type'))
		return JsonResponse({'ready_msg':message})