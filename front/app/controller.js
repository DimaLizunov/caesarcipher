myApp.controller('mainController', ['$scope', '$http', function($scope, $http) {

  $scope.shift = 1;
  $scope.submit = submit;
  $scope.chart = {
    options: {
      scales: {
        yAxes: [{
          display: true,
          ticks: {
            suggestedMin: 0,
            beginAtZero: true,
            stepSize: 1
          }
        }]
      }
    }
  };

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  xhr.open('POST', 'http://127.0.0.1:8000/code/', true)
  

  function sendRequest(model) {
    var requestUrl ='http://127.0.0.1:8000/code/';
    return $http.post(requestUrl, model);
  }

  function submit(type, form) {
    var model;
    $scope.isFormSubmited = true;

    if (form.$invalid) {
      return;
    }

    model = {
      enter_msg: $scope.initText,
      type: type,
      crypt_key: $scope.shift
    }

    getChart($scope.initText);
    checkText($scope.initText);
    sendRequest(model).success(function(response) {
      $scope.resultText = response.ready_msg;
    })
  }

  function checkText(enter_msg) {
    $scope.isTextWarning = true;
  }

  function getChart(enter_msg) {
    var str = enter_msg.trim().replace(/\s+/g, '');
    var result = {};

    $scope.chart.labels = [];
    $scope.chart.data = [];

    angular.forEach(str, function(char) {
      result[char] = result[char] ? ++result[char] : 1;
    });

    angular.forEach(result, function(val, char) {
      $scope.chart.labels.push(char);
      $scope.chart.data.push(val);
    });
  }

}])