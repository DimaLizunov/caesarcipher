myApp.directive('tooltip', [function() {
  var directive = {
    restrict: 'A',
    scope: {},
    link: Link
  };

  function Link($scope, $element) {
    $($element).tooltip()
  }

  return directive;
}])
